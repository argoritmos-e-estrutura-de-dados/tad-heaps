package Heap;

import Item.Item;
import java.util.Random;

/** @author messiah */
public class main {
    public static void main(String[] args) {
        /** Para cada valor de caso de teste (10.000, 20.000, 30.000, 40.000, 50.000,
         * 60.000, 70.000, 80.000, 90.000, 10.000) foi instanciada um novo objeto JHeap.
         */
        for(int k = 0; k < 10; k++){
            System.out.println("\n\nCASO DE TESTE: "+k+"\n");
               
            //Gerar o heap a partir de n elementos ORDENADOS DE FORMA CRESCENTE.
            System.out.println("Gerar heap a partir de n elementos ORDENADOS DE FORMA CRESCENTE:");
            for(int n = 10000; n <= 100000; n += 10000){
                Item[] it = new Item[n];
                for(int i = 1; i < n; i++) it[i] = new Item(i);
                JHeap jh = new JHeap(it, n);
                JHeapSort jhs = new JHeapSort();
                jhs.JHeapSort(jh);
                //System.out.println("\tNúmero de comparações: "+jhs.number_of_comparisons+" Com n:"+(n));
                System.out.println("("+n+","+jhs.number_of_comparisons+")");
            }

            //Gerar o heap a partir de n elementos ORDENADOS DE FORMA DECRESCENTE.
            System.out.println("Gerar heap a partir de n elemento ORDENADOS DE FORMA DECRESCENTE:");
            for(int n = 10000; n <= 100000; n += 10000){
                Item[] it = new Item[n];
                int j = n;
                for(int i = 1; i < n; i++, j--) it[i] = new Item(j);
                JHeap jh = new JHeap(it, n);
                JHeapSort jhs = new JHeapSort();
                jhs.JHeapSort(jh);
                //System.out.println("\tNúmero de comparações: "+jhs.number_of_comparisons+" Com n:"+(n));
                System.out.println("("+n+","+jhs.number_of_comparisons+")");
            }
            
            //Gerar o heap a partir de n elementos ALEATÓRIOS.
            System.out.println("Gerar heap a partir de n elemento ALEATÓRIOS:");
            for(int n = 10000; n <= 100000; n += 10000){
                Item[] it = new Item[n];
                for(int i = 1; i < n; i++) it[i] = new Item((new Random()).nextInt(n));
                JHeap jh = new JHeap(it, n);
                JHeapSort jhs = new JHeapSort();
                jhs.JHeapSort(jh);
                //System.out.println("\tNúmero de comparações: "+jhs.number_of_comparisons+" Com n:"+(n));
                System.out.println("("+n+","+jhs.number_of_comparisons+")");
            }

        }
    }    
}
