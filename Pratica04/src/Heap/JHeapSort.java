
package Heap;
import Item.Item;

public class JHeapSort {
    protected long number_of_comparisons;
    /** O JHeapSort troca a posição do maior elemento do vetor heap, que se 
     * encontra na primeira posição, com o elemento da ultima posição; decrementa-se
     * a última posição e refaz a condição do heap.
     */
    public JHeap JHeapSort(JHeap jheap){
        this.number_of_comparisons = 0;
        JHeap jhp = jheap;
        jhp.number_of_comparisons = 0;
        for(int n = jhp.size()-1; n > 0;){
            Item max = jhp.getItem(1);
            jhp.setItem(1, jhp.getItem(n));
            jhp.setItem(n, max);
            n--;
            jhp.rebuild(1, n);
            this.number_of_comparisons += jhp.number_of_comparisons;
        }
        return jhp;
    }
}
