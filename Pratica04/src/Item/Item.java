package Item;

/**
 * Classe para o tipo genérico.
 * @author messiah
 */
public class Item{
    private int key;
    /** Construtor padrão da classe Item.
     * @param key chave a ser inserida
     */
    public Item(int key){
        this.key = key;
    }
    /** Compara se o parâmetro it é maior ou menor que key. */
    public int compare(Item it){
        if(this.key < it.key) return -1;
        else if(this.key > it.key) return 1;
        return 0;
    }
    public int getKey(){
        return key;
    }
    public void setKey(int k){
        this.key = k;
    }
}
