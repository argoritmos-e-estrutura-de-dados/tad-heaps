
package Heap;

import Item.Item;
/** @author messiah */
public class JHeap {
    /** Tamanho da estrutura heap. */
    private int n;
    /** Vetor do tipo Item genérico, o qual será armazenado os dados da estrutura. */
    private Item[] heap;
    
    protected int number_of_comparisons;
    
    /** Construtor padrão da classe JHeap; neste os valores de parâmetro v e n 
     * são associados aos seus respectivos membros da classe e é ajusatada a 
     * condição do heap a partir do elemento (n/2) +1 até o elemento 1.
     */
    JHeap(Item[] v, int n){
        this.n = n;
        this.heap = new Item[n];
        this.heap = v;
        this.number_of_comparisons = 0;
        
        int left = (n/2)+1;
        while(left > 1){
            left--;
            this.heap = this.rebuild(left, this.n);
        }
    }
    /** O método ajusta a posição 'i' a condição do heap; j recebe o valor da posição
     * '2*i' e 'it' o tipo Item atribuido a essa posição; verifica-se a cada interação 
     * de loop se o valor de j é menor que o tamanho de 'this.heap' e verifica-se 
     * também a condição do heap: 
     *             'this.heap[i] > this.heap[j] && this.heap[i] > this.heap[j+1]"
     * caso essa seja falsa, ocorre a troca entre o max(this.heap[j], this.heap[j+1])
     * e o this.heap[i].
    */
    public Item[] rebuild(int i, int n) {
        int j = i * 2;
        Item it = this.heap[i];
        while(j < n){
            if((j < n) && (this.heap[j].compare(this.heap[j+1]) < 0)){
                j++;
                this.number_of_comparisons++;
            }
            if(it.compare(this.heap[j]) >= 0){
                this.number_of_comparisons++;
                break;
            }
            this.heap[i] = this.heap[j];
            i = j;
            j = i * 2;
        }
        this.heap[i] = it;
        return this.heap;
    }
    /** Retorna o maior item do heap. */
    public Item max(){
        return this.heap[1];
    }
    /** Retorna o tamanho do heap. */
    public int size(){
        return this.n;
    }
    /** Retorna o item i do heap. */
    public Item getItem(int i){
        if(i >= 0 && i < n)
            return this.heap[i];
        return null;
    }
    public void setItem(int i, Item it){
        if(i >= 0 && i < n)
            this.heap[i] = it;
    }
}
